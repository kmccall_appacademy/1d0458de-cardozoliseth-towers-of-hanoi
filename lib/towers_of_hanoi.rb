# Towers of Hanoi
#
# Write a Towers of Hanoi game:
# http://en.wikipedia.org/wiki/Towers_of_hanoi
#
# In a class `TowersOfHanoi`, keep a `towers` instance variable that is an array
# of three arrays. Each subarray should represent a tower. Each tower should
# store integers representing the size of its discs. Expose this instance
# variable with an `attr_reader`.
#
# You'll want a `#play` method. In a loop, prompt the user using puts. Ask what
# pile to select a disc from. The pile should be the index of a tower in your
# `@towers` array. Use gets
# (http://andreacfm.com/2011/06/11/learning-ruby-gets-and-chomp.html) to get an
# answer. Similarly, find out which pile the user wants to move the disc to.
# Next, you'll want to do different things depending on whether or not the move
# is valid. Finally, if they have succeeded in moving all of the discs to
# another pile, they win! The loop should end.
#
# You'll want a `TowersOfHanoi#render` method. Don't spend too much time on
# this, just get it playable.
#
# Think about what other helper methods you might want. Here's a list of all the
# instance methods I had in my TowersOfHanoi class:
# * initialize
# * play
# * render
# * won?
# * valid_move?(from_tower, to_tower)
# * move(from_tower, to_tower)
#
# Make sure that the game works in the console. There are also some specs to
# keep you on the right track:
#
# ```bash
# bundle exec rspec spec/towers_of_hanoi_spec.rb
# ```
#
# Make sure to run bundle install first! The specs assume you've implemented the
# methods named above.

class TowersOfHanoi
  attr_reader :towers
  def initialize
    @towers = [[3,2,1],[],[]]
  end

  def move(from_tower, to_tower)
    move_disc_from = @towers[from_tower]
    move_disc_to = @towers[to_tower]
    move_disc_to << move_disc_from.pop
  end

  def valid_move?(from_tower, to_tower)
    from = @towers[from_tower]
    to = @towers[to_tower]
    unless @towers.include?(from) && @towers.include?(to)
      puts "Choose a valid tower: 0, 1, 2"
      return false
    end
    if from.length > 0 && (to.length == 0 || from[-1] < to[-1])  #it's a valid move
      return true
    end
    puts "Not a valid move, try again"
    return false
  end

  def render
  render = { 1 =>"  #", 2 => " ### ", 3 => "#####  "}
  2.downto(0).each do |idx|
    lines = ""
    @towers.each do |tower|
      if tower[idx] # true
      lines << render[tower[idx]]
      else
        lines << "     "
        lines << " "
      end
    end
    puts lines
  end
    puts "-----   -----   -----\ntower1  tower2  tower3"
  end

  def promt_user
    render
    puts "choose a tower to move from: enter a number 0, 1, or 2 "
    move_from = gets.to_i
    puts "choose a tower to move to: enter a number 0, 1, or 2"
    move_to = gets.to_i
    [move_from,move_to]
  end

  def play
    until won?
      from, to = promt_user
      until valid_move?
        from, to = promt_user
      end
      move(from, to)
      render
    end
    puts "Congratz you won!"
  end

  def won?
    if @towers[0].length == 0
      (1...@towers.length).each do |idx|
        tower = @towers[idx]
        if tower.length == 3 && tower == tower.sort.reverse
          return true
        end
      end
    end
    false
  end

end
